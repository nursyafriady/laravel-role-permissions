<?php

namespace App\Http\Controllers\Permission;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AssignController extends Controller
{
    public function create()
    {
        $roles = Role::get();
        $permissions = Permission::get();
        return view('permission.assigns.create', compact('roles', 'permissions'));
    }

    public function store()
    {
        request()->validate([
            'role' => 'required',
            'permissions' => 'array|required',
        ]);

        $role = Role::find(request('role'));
        // dd($role);
        $role->givePermissionTo(request('permissions'));
        // dd('permissions');

        return back()->with('success', "Permission has been assigned to the $role->name");        
    }

    public function edit(Role $role)
    {
        return view('permission.assigns.sync', [
            'role' => $role,
            'roles' => Role::get(),
            'permissions' => Permission::get(),
        ]);
    }

    public function update(Role $role)
    {
        request()->validate([
            'role' => 'required',
            'permissions' => 'array|required',
        ]);

        $role->syncPermissions(request('permissions'));
        return redirect()->route('assign.create');
    }
}
