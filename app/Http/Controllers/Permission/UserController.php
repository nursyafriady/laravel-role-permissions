<?php

namespace App\Http\Controllers\Permission;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\User;

class UserController extends Controller
{
    public function create()
    {
        $roles = Role::get();
        $users = User::has('roles')->get();
        return view('permission.assigns.user.create', compact('roles', 'users'));
    }

    public function store()
    {
        request()->validate([
            'email' => 'required',
            'roles' => 'array|required',
        ]);

        
        $user = User::where('email', request('email'))->first(); 
        $user->assignRole(request('roles'));
        return back();             
    }

    public function edit(User $user)
    {
        $roles = Role::get();
        $users = User::has('roles')->get();
        $user = $user;
        return view('permission.assigns.user.edit', compact('roles', 'users', 'user'));
    }

    public function update(User $user)
    {
        request()->validate([
            'email' => 'required',
            'roles' => 'array',
        ]);

        $user->syncRoles(request('roles'));
        return redirect()->route('assign.user.create');             
    }
}
