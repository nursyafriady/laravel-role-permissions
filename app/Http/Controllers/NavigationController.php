<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use App\Navigation;

class NavigationController extends Controller
{
    public function create()
    {
        $navigation = new Navigation;
        $permissions = Permission::get();
        $navigations = Navigation::where('url', NULL)->get();
        return view('navigation.create', compact('permissions', 'navigations', 'navigation'));
    }

    public function store()
    {
       request()->validate([
            'name' => 'required',
            'permission_name' => 'required',
        ]);

        Navigation::create([
            'name' => request('name'),
            'url' => request('url') ?? NULL,
            'parent_id' => request('parent_id') ?? NULL,
            'permission_name' => request('permission_name'),
        ]);

        return back();
    }

    public function table()
    {
        $navigations = Navigation::whereNotNull('url')->get();
        return view('navigation.table', compact('navigations'));
    }

    public function edit(Navigation $navigation)
    {
        // dd($navigation);
        $permissions = Permission::get();
        $navigations = Navigation::where('url', NULL)->get();
        // dd($navigations);
        return view('navigation.edit', compact('navigations', 'navigation', 'permissions'));
    }

    public function update(Navigation $navigation)
    {
        $navigation->update([
            'name' => request('name'),
            'url' => request('url') ?? NULL,
            'parent_id' => request('parent_id') ?? NULL,
            'permission_name' => request('permission_name'),
        ]);

        return redirect()->route('navigation.table');
    }

    public function destroy(Navigation $navigation)
    {
        $navigation->delete();
        return redirect()->route('navigation.table');
    }

}
