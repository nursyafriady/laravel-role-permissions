<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // $roles = Role::get();
    // $hasRole = auth()->user()->hasAnyRole($roles);
    // dd($hasRole);
    // $role = Role::whereName('admin')->first();
    // dd($role->givePermissionTo('create category'));
    return view('welcome');
    // $role = Role::find(2);
    // $role->givePermissionTo('create post', 'delete post');
    // dd($role);
});

Auth::routes();

Route::middleware('has.role')->group(function () {
    Route::view('dashboard', 'dashboard')->name('dashboard');

    Route::middleware('permission:create post')->group(function(){
        Route::view('posts/create', 'posts.create');
        Route::view('posts/table', 'posts.table');
        Route::view('categories/create', 'categories.create');
        Route::view('categories/table', 'categories.table');
    });    

    Route::prefix('role-and-permission')->namespace('Permission')->middleware('permission:assign permission')->group(function () {
        Route::get('assign', 'AssignController@create')->name('assign.create');
        Route::post('assign', 'AssignController@store')->name('assign.store');
        Route::get('assign/{role}/edit', 'AssignController@edit')->name('assign.edit');
        Route::put('assign/{role}/edit', 'AssignController@update')->name('assign.update');
        // Route::delete('assign/{id}/delete', 'AssignController@delete')->name('assign.delete');

        // USER
        Route::get('assign/user', 'UserController@create')->name('assign.user.create');         
        Route::post('assign/user', 'UserController@store')->name('assign.user.store');
        Route::get('assign/{user}/user', 'UserController@edit')->name('assign.user.edit');         
        Route::put('assign/{user}/user', 'UserController@update')->name('assign.user.update');

        Route::prefix('roles')->group(function() {
            Route::get('', 'RoleController@index')->name('roles.index');
            Route::post('create', 'RoleController@store')->name('roles.create');
            Route::get('{role}/edit', 'RoleController@edit')->name('roles.edit');
            Route::put('{role}/edit', 'RoleController@update');
            Route::delete('{id}', 'RoleController@delete')->name('roles.delete');
        });

        Route::prefix('permissions')->group(function() {
            Route::get('', 'PermissionController@index')->name('permissions.index');
            Route::post('create', 'PermissionController@store')->name('permissions.create');
            Route::get('{permission}/edit', 'PermissionController@edit')->name('permissions.edit');
            Route::put('{permission}/edit', 'PermissionController@update');
            Route::delete('{id}', 'PermissionController@delete')->name('permissions.delete');
        });          
    });  
    
    Route::prefix('navigation')->middleware('permission:create navigation')->group(function(){
        Route::get('create', 'NavigationController@create')->name('navigation.create');
        Route::post('create', 'NavigationController@store')->name('navigation.store');
        Route::get('table', 'NavigationController@table')->name('navigation.table');
        Route::get('{navigation}/edit', 'NavigationController@edit')->name('navigation.edit');
        Route::put('{navigation}/edit', 'NavigationController@update')->name('navigation.update');
        Route::delete('{navigation}/delete', 'NavigationController@destroy')->name('navigation.delete');
    });
});

Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

