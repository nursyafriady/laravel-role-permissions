<div>
    @can('create post')
    <div class="mb-3">
        <medium class="d-block text-secondary mb-2 text-uppercase">Posts</medium>
        <div class="list-group">
            <a href="#" class="list-group-item list-group-item-action">
                Create New Post
            </a>
            <a href="#" class="list-group-item list-group-item-action">Data Table Posts</a>
        </div>
    </div>
    @endcan

    @can('create category')
    <div class="mb-3">
        <medium class="d-block text-secondary mb-2 text-uppercase">Categories</medium>
        <div class="list-group">
            <a href="#" class="list-group-item list-group-item-action">
                Create Category
            </a>
            <a href="#" class="list-group-item list-group-item-action">Data Table Categories</a>
        </div>
    </div>
    @endcan

    @can('show users')
    <div class="mb-3">
        <medium class="d-block text-secondary mb-2 text-uppercase">Users</medium>
        <div class="list-group">
            <a href="#" class="list-group-item list-group-item-action">
                Create User
            </a>
            <a href="#" class="list-group-item list-group-item-action">Data Table Users</a>
        </div>
    </div>
    @endcan

    @can('assign permission')
    <div class="mb-3">
        <medium class="d-block text-secondary mb-2 text-uppercase">Roles & Permissions</medium>
        <div class="list-group">
            <a href="{{ route('roles.index') }}" class="list-group-item list-group-item-action">Role</a>
            <a href="{{ route('permissions.index') }}" class="list-group-item list-group-item-action">Permissions</a>
            <a href="{{ route('assign.create') }}" class="list-group-item list-group-item-action">Assign Permissions</a>
            <a href="{{ route('assign.user.create') }}" class="list-group-item list-group-item-action">Permission to User</a>
        </div>
    </div>
    @endcan

    @can ('create navigation')
    <div class="mb-3">
        <medium class="d-block text-secondary mb-2 text-uppercase">Navigation Setup</medium>
        <div class="list-group">
            <a href="{{ route('navigation.create') }}" class="list-group-item list-group-item-action">Create New Navigation</a>
            <a href="{{ route('navigation.table') }}" class="list-group-item list-group-item-action">Navigation Table</a>
        </div>
    </div>
    @endcan

    <div class="mb-3">
        <div class="list-group">
            <a class="list-group-item list-group-item-action" href="{{ route('logout') }}" 
               onclick="event.preventDefault();
               document.getElementById('logout-form').submit();"> {{ __('LOGOUT') }} 
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </div>
    </div>

    
</div>