@extends('layouts.backend')

@section('content')
    @if(session('success'))
        <div class="alert alert-success">{{ session('success') }}</div>
    @endif

    <div class="card mb-5">
        <div class="card-header">
            Assign Permission
        </div>
        <div class="card-body">
            <form action="{{ route('assign.update', $role) }}" method="post">
                @method('PUT')
                @csrf
                <div class="form-group">
                    <label for="role">Role Name</label>
                    <select name="role" id="role" class="form-control">
                        <option disabled selected>Choose a Role</option>
                        @foreach($roles as $item)
                            <option {{ $role->id == $item->id ? 'selected' : ''}} value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                    @error('role')
                        <div class="text-danger mt-2 d-block">{{ $message }}</div>
                    @enderror
                </div>

                 <div class="form-group">
                    <label for="permissions">Permissions</label>                    
                    <select name="permissions[]" id="permissions" class="form-control select2" multiple>
                        @foreach($permissions as $permission)
                            <option {{ $role->permissions()->find($permission->id) ? 'selected' : '' }} value="{{ $permission->id }}">{{ $permission->name }}</option>
                        @endforeach
                    </select>
                    @error('permissions')
                        <div class="text-danger mt-2 d-block">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-success">SYNC</button>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $('.select2').select2({
                placeholder : "Select Permissions"
            });
        });
    </script>
@endpush