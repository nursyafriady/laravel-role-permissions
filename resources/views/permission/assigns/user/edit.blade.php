@extends('layouts.backend')

@section('content')
    @if(session('success'))
        <div class="alert alert-success">{{ session('success') }}</div>
    @endif

    <div class="card mb-5">
        <div class="card-header">
            Sync Role for <strong>{{ $user->name }}</strong> 
        </div>
        <div class="card-body">
            <form action="{{ route('assign.user.update', $user) }}" method="post">
                @method('PUT')
                @csrf
                <div class="form-group">
                    <label for="user"> Email User </label>
                    <input type="text" name="email" id="user" class="form-control" value="{{ $user->email }}">
                    @error('email')
                        <div class="text-danger mt-2 d-block">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="roles">Pick Roles</label>
                    <select name="roles[]" id="roles" class="form-control select2" multiple>
                        @foreach($roles as $role)
                            <option {{ $user->roles()->find($role->id) ? 'selected' : '' }} value="{{ $role->id }}">{{ $role->name }}</option>
                        @endforeach
                    </select>
                    @error('roles')
                        <div class="text-danger mt-2 d-block">{{ $message }}</div>
                    @enderror
                </div>
                
                <button type="submit" class="btn btn-success">SYNC</button>
            </form>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $('.select2').select2({
                placeholder : "Select roles"
            });
        });
    </script>
@endpush