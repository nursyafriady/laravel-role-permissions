@extends('layouts.backend')

@section('content')
    @if(session('success'))
        <div class="alert alert-success">{{ session('success') }}</div>
    @endif

    <div class="card mb-5">
        <div class="card-header">
            Pick User By Email Address
        </div>
        <div class="card-body">
            <form action="{{ route('assign.user.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="user">User Name</label>
                    <input type="text" name="email" id="user" class="form-control">
                    @error('email')
                        <div class="text-danger mt-2 d-block">{{ $message }}</div>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="roles">Pick Roles</label>
                    <select name="roles[]" id="roles" class="form-control select2" multiple>
                        @foreach($roles as $role)
                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                        @endforeach
                    </select>
                    @error('roles')
                        <div class="text-danger mt-2 d-block">{{ $message }}</div>
                    @enderror
                </div>
                
                <button type="submit" class="btn btn-success">ASSIGN</button>
            </form>
        </div>
    </div>

     <div class="card">
        <div class="card-header">User & Role Table</div>
        <div class="card-body">
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Role Name</th>
                        <th>Roles</th>
                        <th>Action</th>
                    </tr>
                </thead>                
                <tbody>
                @forelse($users as $index=>$user)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->getRoleNames()->implode(', ')  }}</td>
                        <td>
                            <a href="{{ route('assign.user.edit', $user) }}" class="btn btn-primary btn-sm">SYNC</a>                       
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>Belum Ada Data</td>
                    </tr>
                @endforelse
                </tbody>
            </table>    
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $('.select2').select2({
                placeholder : "Select roles"
            });
        });
    </script>
@endpush