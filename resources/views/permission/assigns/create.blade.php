@extends('layouts.backend')

@section('content')
    @if(session('success'))
        <div class="alert alert-success">{{ session('success') }}</div>
    @endif

    <div class="card mb-5">
        <div class="card-header">
            Assign Permission
        </div>
        <div class="card-body">
            <form action="{{ route('assign.store') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="role">Role Name</label>
                    <select name="role" id="role" class="form-control">
                        <option disabled selected>Choose a Role</option>
                        @foreach($roles as $role)
                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                        @endforeach
                    </select>
                    @error('role')
                        <div class="text-danger mt-2 d-block">{{ $message }}</div>
                    @enderror
                </div>

                 <div class="form-group">
                    <label for="permissions">Permissions</label>                    
                    <select name="permissions[]" id="permissions" class="form-control select2" multiple>
                        @foreach($permissions as $permission)
                            <option value="{{ $permission->id }}">{{ $permission->name }}</option>
                        @endforeach
                    </select>
                    @error('permissions')
                        <div class="text-danger mt-2 d-block">{{ $message }}</div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-success">ASSIGN</button>
            </form>
        </div>
    </div>

    <div class="card">
        <div class="card-header">Role & Permission Table</div>
        <div class="card-body">
            <table class="table table-hover table-bordered">
                <thead class="bg-success">
                    <tr>
                        <th>#</th>
                        <th>Role Name</th>
                        <th>Guard Name</th>
                        <th>Permissions</th>
                        <th>Action</th>
                    </tr>
                </thead>                
                <tbody>
                @forelse($roles as $index=>$role)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $role->name }}</td>
                        <td>{{ $role->guard_name }}</td>
                        <td>{{ $role->getPermissionNames()->implode(', ')  }}</td>
                        <td>
                            <a href="{{ route('assign.edit', $role) }}" class="btn btn-primary btn-sm">SYNC</a>
                            <form action="{{ route('roles.delete', $role) }}" method="POST" class="d-inline">
                                @method('delete')
                                @csrf
                                <button class="btn btn-info btn-sm">DELETE</button>
                            </form>                            
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>Belum Ada Data</td>
                    </tr>
                @endforelse
                </tbody>
            </table>    
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
            $('.select2').select2({
                placeholder : "Select Permissions"
            });
        });
    </script>
@endpush