@extends('layouts.backend')

@section('content')
    <div class="card mb-4">
        <div class="card-header">
            Create New Permission
        </div>
        <div class="card-body">
            <form action="{{ route('permissions.create') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" id="name" class="form-control" value="">
                </div>
                <div class="form-group">
                    <label for="guard_name">Guard Name</label>
                    <input type="text" name="guard_name" id="guard_name" class="form-control" 
                        value="">
                </div>
                <button type="submit" class="btn btn-primary btn-sm">CREATE</button>
            </form>   
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            Table Of Permissions
        </div>
        <div class="card-body">
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Guard Name</th>
                        <th>Created at</th>
                        <th>Updated at</th>
                        <th>Action</th>
                    </tr>
                </thead>                
                <tbody>
                @forelse($permissions as $index=>$permission)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $permission->name }}</td>
                        <td>{{ $permission->guard_name }}</td>
                        <td>{{ $permission->created_at->format("d F Y") }}</td>
                        <td>{{ $permission->updated_at->format("d F Y") }}</td>
                        <td>
                            <a href="{{ route('permissions.edit', $permission) }}" class="btn btn-primary btn-sm">EDIT</a>
                            <form action="{{ route('permissions.delete', $permission) }}" method="POST" class="d-inline">
                                @method('delete')
                                @csrf
                                <button class="btn btn-info btn-sm">DELETE</button>
                            </form>
                            
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td>Belum Ada Data</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection