@extends('layouts.backend')

@section('content')
    <div class="card">
        <div class="card-header">
            Navigation Table
        </div>
        <div class="card-body">
           <table class="table table-hover table-bordered">
                <tr class="bg-success">
                    <th>Parent</th>
                    <th>Navigation Name</th>
                    <th>URL LINK</th>
                    <th>Permission Name</th>
                    <th>Action</th>
                </tr>
                @foreach($navigations as $navigation)
                    <tr>
                        <th>{{ $navigation->parent->name ?? ''}}</th>
                        <th>{{ $navigation->name }}</th>
                        <th>{{ $navigation->url ?? "It's a Parent" }}</th>
                        <th>{{ $navigation->permission_name }}</th>
                        <th>
                            <a href="{{ route('navigation.edit', $navigation) }}">Edit Or Remove</a>
                        </th>
                    </tr>
                @endforeach
           </table>
        </div>
    </div>
@endsection